#!/usr/bin/python3
##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################

# Script to generate GitLab CI configuration

import sys

if len(sys.argv) != 2:
    print("Usage: genci.py <path-to-programs.txt>")
    exit()

header = """variables:
  DOCKER_DRIVER: overlay2
  GIT_STRATEGY: fetch
  PARAMS: MOPSA_ROOT=./mopsa-analyzer SRC=/home/mopsa/src/coreutils-8.30

image: mopsa-coreutils

stages:
- Build
- Prepare
- Analysis

mopsa:
  stage: Build
  script:
  - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/mopsa/mopsa-analyzer.git
  - cd mopsa-analyzer
  - eval `opam config env`
  - ./configure
  - make -j
  artifacts:
    paths:
    - ./mopsa-analyzer/bin/mopsa
    - ./mopsa-analyzer/bin/mopsa-c
    - ./mopsa-analyzer/bin/mopsa.bin
    - ./mopsa-analyzer/bin/mopsa-build
    - ./mopsa-analyzer/bin/mopsa-wrappers
    - ./mopsa-analyzer/analyzer/_build/mopsa.native
    - ./mopsa-analyzer/parsers/c/_build/wrapper.native
    - ./mopsa-analyzer/_build/default/analyzer/mopsa.exe
    - ./mopsa-analyzer/_build/default/parsers/c/bin/build_wrapper.exe
    - ./mopsa-analyzer/share
    expire_in: 4h

database:
  stage: Prepare
  script:
  - make prepare $PARAMS
  artifacts:
    paths:
    - ./mopsa.db
    expire_in: 4h

.analysis:
  stage: Analysis
  script:
  - make $TARGET/no-arg/a1 $PARAMS"""

programs = []
with open(sys.argv[1]) as f:
    programs = f.read().splitlines()


keywords = ["false", "true", "yes"]

def sanitize(s):
    if s in keywords:
        return '"%s"'%(s)
    else:
        return s

body = ["""%s:
  variables:
    TARGET: %s
  extends: .analysis"""%(sanitize(p),sanitize(p)) for p in programs]

print(header)
print("\n\n".join(body))
